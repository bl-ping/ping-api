package com.statefarm.pingpong;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.GregorianCalendar;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PingPongController.class)
public class PingPongControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PingPongService pingPongService;

    Date testDate;

    @Test
    void getTimeStampReturnsTimeStamp() throws Exception {
        testDate = new Date();
        System.out.println(testDate);
        when(pingPongService.getTimeStamp()).thenReturn(testDate);
        mockMvc.perform(get("/api/pingpong"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(String.format("%s",testDate)));
    }
}
