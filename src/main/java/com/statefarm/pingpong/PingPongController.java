package com.statefarm.pingpong;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class PingPongController {

    PingPongService pingPongService;

    public PingPongController(PingPongService pingPongService) {
        this.pingPongService = pingPongService;
    }

    @CrossOrigin(origins = "https://ping-pong-time-ui.herokuapp.com/")
    @GetMapping("/api/pingpong")
    public ResponseEntity<String> getTimeStamp() {
        Date date = pingPongService.getTimeStamp();
        return date == null ? ResponseEntity.status(500).build() : ResponseEntity.ok(date.toString());
    }
}
